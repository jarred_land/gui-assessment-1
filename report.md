# Assessment 1
## Jarred Land - 10008790

---

## Good GUI Practices

Good practices in GUI programming can make code very easy to read and work with, 
while improper usage of conventions can make reading code awful and impractical.
This report will detail some of the generally useful things to remember when
programming GUI based apps.

### *K.I.S.S*-                                                        *Source: 1*                            
Keep It Simple Stupid. This practice involves not overcomplicating things and making 
your workload easier on yourself as well as others who may want to use your code in
the future. I will incorporate this idea into the making of my app by keeping the code 
simple and easy to work with.

### Reduction of learning curve for a new user-                       *Source: 1*   
   This is important as I do not want a new user to be confused by my app. To
   prevent this from happening, I will keep only the bare minimum on screen, only 
   the instructions for the color and three sliders for adjustment.

### Following basic design principles-                                *Source: 1*   
   This refers to the practice of taking into account how my app will look and 
   whether or not it is intuitive enough for the average user to use. To make use 
   of this idea, I will ensure that my objects in the app are spaced correctly and
   aligned in a way that makes sense for the type of application that it is.

### Consistency-                                                       *Source: 1*    
   I aim to maintain a level of consistency in the way my program looks and they 
   way the code is structured. This is important because it will be more visually 
   appealing and also easier for me to edit and make changes if the places that 
   pieces of code are stored in are standardized.

### Use of color and contrast-                                         *Source: 2*   
   As my app will allow the user to change the color of the background of the app,
   I need my confirm and reset buttons to stand out against the background. To do 
   this I will encase my buttons in a white box that does not change color. 
   This will keep my text legible at all times.
### Font-                                                              *Source: 2*   
   Text must always be legible on any resoloution and it should be kept simple and
   easy on the eyes. To keep my fonts pleasant to look at, I will use a simple sans
   serif font and keep the size at around 12-14, as this is not too small or too big.
   
### Forgiving inputs-                                                  *Source: 3*   
   Forgiving inputs is the idea that the input of a user should not be too stringent,
   as the average user cannot be expected to know the exact input method required. To 
   solve for this, I will not include text fields or the ability for a user to enter
   incorrect values. By keeping the input as sliders only, the user doesn't have to think
   too much and it is much harder to break the application.
   
### Don't reinvent the wheel-                                          *Source: 4*   
   This phrase means that it is unneccesary to always be innovating, especially in the
   case of simple apps like this one. Though I am tempted to add things like the option
   to save user colors, it is not required and will cost me time that would be better
   spent ensuring that my application does exactly what it needs to do, no more and no 
   less. However, it may be beneficial to add other simple color changing options, as long
   as they are not too time consuming, as they will keep the user interested in the application 
   for longer.

### Peer Review
For the most part, your report is well written, with clear, concise detail, consistent 
links that confirm your research,and you have not over or underwritten about any points 
in particular. I would suggest adding a few more technical details about GUI programming 
to your points, to make it more in line with academic writing. Also, you appear to have 
introduced new information in your conclusion, which I would not recommend. Other than 
that, the report should hold well under scrutiny.

-Jason Wallace 

## Conclusion-                                                         *Source: 5*   

With these GUI practices taken into account, I believe that my application will work 
well and complete the task it needs to do with efficiency and in an easy to understand way.
The user will be able to tell exactly what they need to do and how, as they will be given a 
small set of simple instructions in the app. I have decided not to use a confirmation button,
as I feel that it is much more user friendly if the color changes as the user makes adjustments,
rather than leaving the user not knowing what color they are creating until the very end. I got 
this idea from source 3, where teh article talks about making sure that the system communicates
what is happening to the user and I don't feel that leaving the user unaware of their color until
the end allows this.

---

# Sources

1. https://stackoverflow.com/questions/90813/best-practices-principles-for-gui-design

2. https://www.elegantthemes.com/blog/resources/10-rules-of-good-ui-design-to-follow-on-every-web-design-project

3. https://www.usability.gov/what-and-why/user-interface-design.html

4. http://beijerinc.com/pdf/whitepaper/interface_design_best_practices.pdf

5. https://webflow.com/blog/10-essential-ui-design-tips